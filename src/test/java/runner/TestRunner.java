package java.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestRunner {
    public static WebDriver driver;

    @BeforeClass
    public static void setup() {
        System.out.println("====================================================");
        System.out.println("Starting WebDriver");
        driver = new ChromeDriver();
        System.out.println("Maximizing browser window");
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void tearDown() {
        System.out.println("Closing WebDriver");
        System.out.println("====================================================");
        driver.quit();
    }
}
